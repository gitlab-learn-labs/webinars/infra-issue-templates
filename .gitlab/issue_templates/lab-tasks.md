### Lab Mega-Issue

This issue will cover all the tasks a DA or DA Partner needs to complete in order to prep for an upcoming FM Workshop

## Lab Info

- Type:

- Date:

- Region: 

- Attendance:

## TODO's

# Initial Tasks

- [ ] Hold initial content discussion/planning

# Post Handoff

- [ ] Create custom content (use templates where we can)

- [ ] Created testing code on gitlabdemo.com

- [ ] Create new slide deck (use existing if we can)

- [ ] Send to presenter

# Final Prep

- [ ] Create production code, update slides

- [ ] Spin up shared runners

- [ ] Add staff to created group

**If Instruqt**

- [ ] Prep hot start

- [ ] Create shareable link, add to slides

# Post Lab

- [ ] Gather feedback

/label ~"Lab Task"
/label ~"SA"
/cc @lfstucker