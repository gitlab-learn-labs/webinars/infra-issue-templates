### Workshop Tracking Issue

Tracking issue for an internal workshop

## Workshop Info

- Type:

- Date:

- Region: 

## Staff

Presenter:

QA:

QA:

QA:

## Content

Slide Deck:

Project:

Test Code:

Prod Code:

## TODO's

# Initial Task

- [ ] Created testing code on gitlabdemo.com

- [ ] Create new slide deck

- [ ] Hold initial content handoff

# Post Handoff

- [ ] Run through content 1 week out

# Final Prep

- [ ] Create production code, update slides

- [ ] Create shareable slide deck

- [ ] Spin up shared infra

- [ ] Add staff to created group

- [ ] Prep messages (Intro, links to content, etc)

# During Workshop

- [ ] Ensure platform stays stable, help with management and QA

# Post Workshop

- [ ] Fix any issues found during live session

- [ ] Get recording and add it to doc + slack


/label ~"Workshop Task"
/label ~"SA"
/cc @lfstucker