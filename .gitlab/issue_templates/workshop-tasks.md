### Workshop Mega-Issue

This issue will cover all the tasks a DA or DA Partner needs to complete in order to prep for an hands on lab

## Workshop Info

- Type:

- Date:

- Region: 

## TODO's

# Initial Task

- [ ] Identify presenters, create slack group

- [ ] Created testing code on gitlabdemo.com

- [ ] Create new slide deck

- [ ] Hold initial content handoff

# Post Handoff

- [ ] Run through content 1 week out

# Final Prep

- [ ] Get final attendance estimate

- [ ] Create production code, update slides

- [ ] Create shareable slide deck

- [ ] Spin up shared 

- [ ] Add staff to created group

- [ ] Prep messages (Intro, links to content, etc)

**If Instruqt**

- [ ] Prep hot start

- [ ] Create sharbale link, add to slides

- [ ] Ensure presenters grab hot start 5 minutes before workshop starts

# During Workshop

- [ ] Ensure platform stays stable, help with management and QA

# Post Workshop

- [ ] Fix any issues found during live session

- [ ] Get recording and add it to doc

- [ ] Send recording cut off time to FM team

**If Instruqt**

- [ ] Gather and send out course metrics

/label ~"Workshop Task"
/label ~"SA"
/label ~"TM & FM"
/cc @lfstucker