## Application Request

(Check that there already isnt an open/closed issue about this infra [here](https://gitlab.com/gitlab-learn-labs/webinars/dev-issue-board))

Please provide relevant information about the infra you are requesting below. Link any relevant docs and be sure to explain why you think this shared infra will be valuable to the wider CS org.

---

## Your Info

(Please provide your GitLab ID and Slack handle )



/label ~"Learn Labs New Infra"
/label ~"DA Partner Potential"
/label ~"SA"
/cc @lfstucker
