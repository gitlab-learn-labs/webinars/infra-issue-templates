## Application For Enhancement

(Current Options Are: Grafana | Hashi Vault | Jenkins | Artifactory)
(If you dont see the app you might want please open an issue [here](https://gitlab.com/gitlab-learn-labs/webinars/dev-issue-board))

## Summary

(Please provide an in depth description of what you want added/changed to the listed application. Provide links/screenshots where necessary)



/label ~enhancement
/label ~"CS-SDS Infra Update"
/label ~"DA Partner Potential"
/label ~"SA"
/cc @lfstucker
/assign @lfstucker
